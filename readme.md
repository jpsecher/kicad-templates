# KiCAD templates

This repository holds mainly unpopulated PCBs that will fit various readily available project enclosures.  If applicable, the mounting holes are made with plated through holes.

To use a template, first create a KiCAD project the way you would normally do, and then overwrite the `.kicad_pcb` file with the template PCB that fits your enclosure.

The `unverified` directory contains PCBs that have not yet been physically verified, so use with care.

In general, use at your own risk.
